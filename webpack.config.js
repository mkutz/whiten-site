var webpack = require('webpack');

module.exports = {
    devtool: 'eval',
    entry: {
        index: './src/front/index.js'
    },
    output: {
        filename: './public/assets/bin/[name].js'
    },
    module: {
        loaders: [
            {
                test: /front(\/|\\).*\.js$/,
                loader: 'babel',
                query: {
                    presets: [ 'es2015', 'stage-0', 'react' ]
                }
            }
        ]
    },
    plugins: [
    ]
};
