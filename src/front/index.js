import React from 'react'
import { render } from 'react-dom';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      packages: '',
      registry: '',
      link: 'about:blank',
    };
    this.handleRegistryChange = this.handleRegistryChange.bind(this);
    this.handlePackagesChange = this.handlePackagesChange.bind(this);
    this.handleDownload = this.handleDownload.bind(this);
  }

  handlePackagesChange(event){
    this.setState({
      packages: event.target.value,
    })
  }

  handleRegistryChange(event){
    this.setState({
      registry: event.target.value,
    })
  }

  handleDownload(){
    window.location = ('api/' + this.state.registry + '/' + this.state.packages.replace(' ', ';'));
  }

  render(){
    return (
      <div>
        <input 
          type='text'
          placeholder='mvn, npm or apm'
          value={ this.state.registry }
          onChange={ this.handleRegistryChange }
        />
        <input 
          type='text'
          placeholder='react react-dom'
          value={ this.state.packages }
          onChange={ this.handlePackagesChange }
        />
        <input 
          type='button'
          value='download'
          onClick={ this.handleDownload }
        />
      </div>
    );
  }
}

render( <App />, document.getElementById('app') );
