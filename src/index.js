import Express from 'express';
import { join } from 'path';
import routes from './back/route.js';

let app = Express();

app.use('/api', routes);
app.use('/', Express.static(join(__dirname, '../public')));

app.listen(8081, () => {
    console.log('listening');
});
