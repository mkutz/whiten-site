import whiten from 'whiten';
import mvnWhiten from 'mvn-whiten';

export default function download ( registry, packages, cb ) {
  getDownloader(registry).call(null, {
    packages,
    cb,
  });
}

function defaultDownloader({ cb }) {
  cb(new Error('shitty shit'));
}

function getDownloader(registry) {
  switch( registry ){
    case 'npm': 
      return npmDownload;
    case 'apm':
      return apmDownload;
    case 'mvn':
      return mvnDownload;
    default:
      return defaultDownloader;
  }
}

function mvnDownload ( { packages, cb } ) {
  console.log('in mvn');
  mvnWhiten( packages, cb)
}

function napmDownload ( registry, { packages, cb } ) {
  console.log('packages are ' + typeof packages);
  whiten( packages, registry, ( err, tar, deleteTmp ) => {
    cb( err, { tar, cb: deleteTmp } );
  } );
}

let npmDownload = napmDownload.bind(null, 'npm');
let apmDownload = napmDownload.bind(null, 'apm');
