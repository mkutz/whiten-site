import { Router } from 'express';
import download from './download.js';
import validate from 'validate-npm-package-name';

let route = Router();
const notFoundError = /404 no such package available : (.*)/;

route.get('/:registery/:packages', ( req, res ) => {
    let packages = [].concat(req.params.packages.split(';'));
    for( let i = 0; i < packages.length; i++ ){
      const errors = validate(packages[i]).errors;
      if ( errors || (/('|")/).test(packages[i]) ) {
        res.status(403).send(`Bad package name "${ packages[i] }"`);
        return;
      }
    }
    download( req.params.registery, packages, ( err, { tar, cb } ) => {
      if ( err ) {
        if ( notFoundError.test(err.message) ) {
          res.status(404).send('Package "' + notFoundError.exec( err.message )[1] + '" not found!');
        }
        else {
          res.status(500).send('Some unknown error occered. Please check that the name of the packages are correct\n' + err.message);
        }
        return;
      }
      if ( tar === undefined ){
        res.status(500).send('error occured while downloading. The stream is empty');
        return;
      }
      if ( cb === undefined ){
        res.status(500).send('error occured while downloading. The callback is empty');
        return;
      }
      setFilename( { res, filename: 'whiten.tar' } );
      tar.pipe(res).on('end', cb);
      req.on('end', cb);
      res.on('end', cb);
    } );
})

function setFilename ( { res, filename } ){
  res.setHeader( 'Content-disposition', 'attachment; filename=' + filename );
}

export default route;
