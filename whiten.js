import ramda from 'ramda';
import { exec } from 'shelljs';

function whiten (type, packages, cb){
    let command = 'whiten-mvn';
    if ( type === 'apm' || type === 'npm' ){
        command = 'whiten';
    }
    if ()
    exec()
}
let get = ramda.curry((type, packages, cb) => {
    whiten(packages, type, cb);
})

module.exports.npm = get('npm');

module.exports.apm = get('apm');
